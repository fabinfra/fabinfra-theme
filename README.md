# Important note
The gitlab.io page was cancelled times ago. Therefore this archive is going to be archived too

# FabInfra - Theme
Responsive Theme without Javascript. It belongs to gitlab.io pages (see https://gitlab.com/fabinfra/fabinfra.gitlab.io). 

## Main Colors
### Primary
RGB: #00d4aa
CMYK: C: 68,27% M: 00,00% Y: 47,93% K: 00,00%
HKS: 53
Pantone: 2239C
RAL (best alternative): 6016

### Secondary
RGB: #3c474d
CMYK: C: 73,04% M: 55,09% Y: 49,02% K: 47,64%
HKS: 93
Pantone: 431C
RAL (best alternative): 7026b

## Sponsors Logo
880px x 625px